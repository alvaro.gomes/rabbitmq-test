package br.com.ajbg;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

import java.io.IOException;

public class App {
	public static void main( String[] args ) throws Exception {
		// Setup and open connection
		ConnectionFactory factory = new ConnectionFactory();
		factory.setUsername("user");
		factory.setPassword("pass");
		factory.setHost("rabbitmq.server.com");
		factory.setPort(5672);
		factory.setVirtualHost("/");
		Connection conn = factory.newConnection();

		// Open channel and setup routing
		Channel channel = conn.createChannel();
		channel.exchangeDeclare("exchange0", "direct", true);
		String queueName = channel.queueDeclare().getQueue();
		channel.queueBind(queueName, "exchange0", "all");

		// Consuming messages
		channel.basicConsume(queueName, true, "myConsumerTag",
				new DefaultConsumer(channel) {
			private int count = 0;
			@Override
			public void handleDelivery(String consumerTag,
					Envelope envelope,
					AMQP.BasicProperties properties,
					byte[] body) throws IOException {
				System.out.println("Message: " + count++);
			}
		});

		byte[] message = new byte[1024];
		// Sending messages
		for (int i=0; i<100000; i++) {
			channel.basicPublish("exchange0", "all", null, message);
		}
	}
}
